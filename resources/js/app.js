require("./bootstrap");
import Vue from "vue/dist/vue";
import router from "./routes";
import VueRouter from "vue-router";

window.Vue = require("vue");
Vue.use(VueRouter);

const app = new Vue({
    el: "#app",
    router,
    mounted() {
        let externalScript = document.createElement("script");
        externalScript.setAttribute("src", "/js/qz-tray.js");
        document.head.appendChild(externalScript);
    }
});
