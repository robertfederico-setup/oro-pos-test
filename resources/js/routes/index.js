import VueRouter from "vue-router";
import BarcodeTest from "../components/BarcodeTest";

const routes = [
    {
        path: "/",
        component: BarcodeTest,
        name: "barcode"
    }
];

const router = new VueRouter({
    mode: "history",
    routes
});

export default router;
