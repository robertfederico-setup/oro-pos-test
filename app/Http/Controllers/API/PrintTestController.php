<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Branch;
use App\Models\User;
use App\Models\BranchTerminal;

use charlieuki\ReceiptPrinter\ReceiptPrinter as ReceiptPrinter;

class PrintTestController extends Controller
{
    public function print()
    {
        $branch = Branch::find(1)->terminals;
        $user = User::all();
        $branch_terminal = BranchTerminal::all();

        foreach ($branch as $b) {
            if ($b->computer_username === 'Robert-Laptop') {
                $terminal = [
                    'branch' => $b->branch_id,
                    'computer_username' => $b->computer_username,
                    'printer_device_name' => $b->printer_device_name
                ];

                $computer_name = $b->computer_username;
                $printer_device_name = $b->printer_device_name;
            }
        }

        // Set params
        $mid = '123123456';
        $store_name = 'YOURMART';
        $store_address = 'Mart Address';
        $store_phone = '1234567890';
        $store_email = 'yourmart@email.com';
        $store_website = 'yourmart.com';
        $tax_percentage = 10;
        $transaction_id = 'TX123ABC456';
        $currency = 'P ';

        // Set items
        $items = [
            [
                'name' => 'French Fries (tera)',
                'qty' => 2,
                'price' => 65000,
            ],
            [
                'name' => 'Roasted Milk Tea (large)',
                'qty' => 1,
                'price' => 24000,
            ],
            [
                'name' => 'Honey Lime (large)',
                'qty' => 3,
                'price' => 10000,
            ],
            [
                'name' => 'Jasmine Tea (grande)',
                'qty' => 3,
                'price' => 8000,
            ],
        ];

        // Init printer
        $printer = new ReceiptPrinter;
        $printer->init('windows', '');

        // Set store info
        $printer->setStore($mid, $store_name, $store_address, $store_phone, $store_email, $store_website);

        // Set currency
        $printer->setCurrency($currency);

        // Add items
        foreach ($items as $item) {
            $printer->addItem(
                $item['name'],
                $item['qty'],
                $item['price']
            );
        }
        // Set tax
        $printer->setTax($tax_percentage);

        // Calculate total
        $printer->calculateSubTotal();
        $printer->calculateGrandTotal();

        // Set transaction ID
        $printer->setTransactionID($transaction_id);

        // Set qr code
        $printer->setQRcode([
            'tid' => $transaction_id,
        ]);

        // Print receipt
        $printer->printReceipt();

        // return response()->json([
        //     'terminal' => $terminal
        // ]);
    }

    public function show()
    {
    }
}
