<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\BranchTerminal;

class BranchTerminalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BranchTerminal::create([
            'computer_username' => 'Robert-Laptop',
            'printer_device_name' => 'EPSON TM-T88IV ReStick',
            'branch_id' => 1,
        ]);

        BranchTerminal::create([
            'computer_username' => 'Robert-Laptop_2',
            'printer_device_name' => 'EPSON TM-T88IV ReStick',
            'branch_id' => 1,
        ]);
    }
}
